package com.spengergasse.dockerservice.domaincommons.display.user;

import com.spengergasse.dockerservice.domaincommons.display.BaseDomain;
import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table
public class User extends BaseDomain<User> implements UserDetails {

    @NotNull
    @NonNull
    private String username;

    @NotNull
    @NonNull
    private String hashedPassword;

    @OneToMany(fetch = FetchType.EAGER)
    private final List<Role> roles = new ArrayList<>();

    @Transient
    private final List<Group> groups = new ArrayList<>();

    @Transient
    private final List<Container> containers = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }

    @Override
    public String getPassword() {
        return getHashedPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public int compareTo(User o) {
        return getUsername().compareToIgnoreCase(o.getUsername());
    }
}
