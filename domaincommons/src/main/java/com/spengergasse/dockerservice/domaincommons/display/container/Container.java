package com.spengergasse.dockerservice.domaincommons.display.container;

import com.spengergasse.dockerservice.domaincommons.display.BaseDomain;
import com.spengergasse.dockerservice.domaincommons.display.user.User;
import lombok.*;

import java.security.acl.Group;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Container{

    private String name;
    private Image image;
    private User creator;
    private List<Group> group;

}
