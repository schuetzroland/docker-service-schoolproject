package com.spengergasse.dockerservice.domaincommons.display.container;

import com.spengergasse.dockerservice.domaincommons.display.user.User;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ContainerGroup {

    private String name;
    private User user;
    private List<Container> containers;

}
