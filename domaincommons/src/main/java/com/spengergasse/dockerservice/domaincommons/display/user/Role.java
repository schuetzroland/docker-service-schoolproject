package com.spengergasse.dockerservice.domaincommons.display.user;

import com.spengergasse.dockerservice.domaincommons.display.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Role extends BaseDomain<Role> implements GrantedAuthority {

    @NotNull
    private String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    @Override
    public int compareTo(Role o) {
        return getAuthority().compareToIgnoreCase(o.getAuthority());
    }
}
