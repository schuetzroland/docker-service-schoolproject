package com.spengergasse.dockerservice.domaincommons.display.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:/application-domain.properties")
public class DomainConfig {
}
