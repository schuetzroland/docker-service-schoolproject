package com.spengergasse.dockerservice.domaincommons.display.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RepositorySource {

    private String siteName;
    private String repoName;
    private String siteUrl;
    private String repoUrl;

}
