package com.spengergasse.dockerservice.domaincommons.display.container;

import com.spengergasse.dockerservice.domaincommons.display.repository.RepositorySource;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
class Image {

    private String version;
    private RepositorySource source;

}
