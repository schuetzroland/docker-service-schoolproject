package com.spengergasse.dockerservice.dockerentity.service;

import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ExecCreation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DockerEntityService {

    Container stopContainer();

    Container startContainer();

    String executeCommand(String command);

    Container getInfo();
}
