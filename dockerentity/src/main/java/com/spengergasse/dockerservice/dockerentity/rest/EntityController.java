package com.spengergasse.dockerservice.dockerentity.rest;

import com.spengergasse.dockerservice.dockerentity.service.DockerEntityService;
import com.spengergasse.dockerservice.dockerentity.service.DockerEntityServiceImpl;
import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import com.spengergasse.dockerservice.restcommons.domain.response.StatusResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping(path = "/api/entity")
public class EntityController {

    private final DockerEntityService dockerEntityService;


    //TODO response
    @PostMapping("/start")
    public ResponseEntity<StatusResponse> start() {
        Container container = dockerEntityService.startContainer();
        return ResponseEntity.ok(StatusResponse.builder()
                .build());
    }
    @PostMapping("/stop")
    public ResponseEntity<String> stop(){
        dockerEntityService.stopContainer();
        return ResponseEntity.ok("Ok");
    }

    //TODO
    @GetMapping("/info")
    public ResponseEntity<String> info(){
        return ResponseEntity.ok("Ok");
    }

    //TODO
    @PostMapping("/command")
    public ResponseEntity<String> executeCommand(){
        return ResponseEntity.ok("Ok");
    }
}
