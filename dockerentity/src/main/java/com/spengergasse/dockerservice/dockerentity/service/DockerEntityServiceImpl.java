package com.spengergasse.dockerservice.dockerentity.service;

import com.spengergasse.dockerservice.dockerentity.config.ContainerProviderConfig;
import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ExecCreation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Component
@Profile("prod")
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DockerEntityServiceImpl implements DockerEntityService {

    private final DefaultDockerClient dockerClient;
    private final ContainerProviderConfig.ContainerProvider containerProvider;


    //TODO return value
    @Override
    public Container stopContainer() {
        try {
            dockerClient.stopContainer(containerProvider.getContainerId(), 30);
            return null;
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    //TODO return value
    @Override
    public Container startContainer() {
        try {
            dockerClient.startContainer(containerProvider.getContainerId());
        return null;
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String executeCommand(String command) {
        try {
            final String[] commandQuery = {"sh", "-c", command};
            final ExecCreation execCreation = dockerClient.execCreate(
                    containerProvider.getContainerId(), commandQuery, DockerClient.ExecCreateParam.attachStdout(),
                    DockerClient.ExecCreateParam.attachStderr());

            final LogStream output = dockerClient.execStart(execCreation.id());

            return output.readFully();

        } catch (InterruptedException | DockerException e) {
            throw new RuntimeException(e);
        }
    }

    //TODO
    @Override
    public Container getInfo() {
        return null;
    }
}
