package com.spengergasse.dockerservice.dockerentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class DockerEntityApplication {

    public static void main(String[] args){
        SpringApplication.run(DockerEntityApplication.class,args);
    }
}
