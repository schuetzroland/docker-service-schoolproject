package com.spengergasse.dockerservice.dockerentity.config;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

import static com.spengergasse.dockerservice.dockerentity.config.ContainerProviderConfig.*;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Profile("prod")
public class Initializer {

    @Value("${image}")
    private String image;

    @Value("${env}")
    private List<String> env;

    private final DefaultDockerClient docker;
    private final ContainerProviderConfig.ContainerProvider containerProvider;

    @PostConstruct
    public void doSomething() throws DockerException, InterruptedException {
      log.info("After Init");

      //TODO real image
      if(docker.listImages(DockerClient.ListImagesParam.allImages()).stream().noneMatch(x -> x.repoTags().contains("mongo:3.0.15-wheezy"))){
          docker.pull(image);
      }
      String id = docker.createContainer(ContainerConfig.builder()
              .image(image)
              .env(env)
              .build()).id();
      docker.startContainer(id);

      containerProvider.setContainerId(id);
    }
}
