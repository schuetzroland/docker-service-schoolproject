package com.spengergasse.dockerservice.dockerentity.config;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;


@Configuration
public class ContainerProviderConfig {

    @Scope("singleton")
    @Bean
    public ContainerProvider getContainerProvider(){
        return new ContainerProvider();
    }

    @Data
    public class ContainerProvider{
        private String containerId;
    }
}
