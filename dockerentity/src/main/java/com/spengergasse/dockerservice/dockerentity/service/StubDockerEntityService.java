package com.spengergasse.dockerservice.dockerentity.service;

import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Profile("dev")
public class StubDockerEntityService implements DockerEntityService{
    @Override
    public Container stopContainer() {
        log.info("DockerEntityService: stopContainer()");
        return null;
    }

    @Override
    public Container startContainer() {
        log.info("DockerEntityService: startContainer()");
        return null;
    }

    @Override
    public String executeCommand(String command) {
        log.info("DockerEntityService: executeCommand({})",command);
        return null;
    }

    @Override
    public Container getInfo() {
        log.info("DockerEntityService: getInfo()");
        return null;
    }
}
