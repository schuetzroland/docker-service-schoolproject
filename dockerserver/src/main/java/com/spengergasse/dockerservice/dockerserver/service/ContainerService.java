package com.spengergasse.dockerservice.dockerserver.service;

import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.exceptions.DockerException;

import java.util.List;

public interface ContainerService {
    Container createContainer(String image, List<String> env);

    List<Container> listContainers();

    Container stopContainer(String containerId);

    Container startContainer(String containerId);

    String executeCommand(String containerId, String command);

    Container getInfos(String containerId);

}
