package com.spengergasse.dockerservice.dockerserver.rest;

import com.spengergasse.dockerservice.dockerserver.service.ContainerService;
import com.spengergasse.dockerservice.restcommons.domain.requests.CustomCommandRequest;
import com.spengergasse.dockerservice.restcommons.domain.response.*;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(path = "/api/docker/container")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ContainerController {

    private final ContainerService containerService;

    //TODO Create image
    @PostMapping("/create")
    public ResponseEntity<ContainerCreateResponse> createContainer() {
        log.info("Create container");
        containerService.createContainer("",null);
        /*ContainerCreation var = dockerClient.createContainer(ContainerConfig.builder()
                .image("nginx:alpine")
                .build());*/


        return ResponseEntity.ok(ContainerCreateResponse.builder()
                .id("Test"/*var.id()*/)
                .build());
    }

    //TODO implement
    @GetMapping(path = "/list")
    public ResponseEntity listContainers() {
        containerService.listContainers();
        throw new NotImplementedException();
    }



    @PostMapping(path = "/{id}/stop")
    public ResponseEntity<StatusResponse> stopContainer(@PathVariable("id") String id) throws DockerException, InterruptedException {
        log.info("Stopping container {}",id);

        containerService.stopContainer(null);

        return ResponseEntity.ok(StatusResponse.builder()
                .id(id)
                .containerId(id)
                .status(Status.STOPPED)
                .build());
    }

    @PostMapping(path = "/{id}/start")
    public ResponseEntity<Void> startContainer(@PathVariable("id") String id) throws DockerException, InterruptedException {
        log.info("Start container");
        containerService.startContainer(null);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/commands")
    public ResponseEntity<String> executeCommand(@PathVariable("id") String id, @RequestBody CustomCommandRequest customCommandRequest) throws DockerException, InterruptedException {
        log.info("Exec command in container");
        String execOutput = containerService.executeCommand(null, null);

        return ResponseEntity.ok(execOutput);
    }

    @GetMapping("/{id}/info")
    public ResponseEntity<InfoResponse> getInfo(@PathVariable("id") String id){

        containerService.getInfos(null);
        return ResponseEntity.ok(InfoResponse.builder()
                .infos("Test")
                .build());
    }

}
