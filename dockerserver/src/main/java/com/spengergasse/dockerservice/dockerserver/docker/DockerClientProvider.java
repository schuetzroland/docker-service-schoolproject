package com.spengergasse.dockerservice.dockerserver.docker;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerCertificates;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Paths;

@Slf4j
@Configuration
public class DockerClientProvider {

    @Value("${docker.cert_path}")
    String path;


    //TODO not sure about the local ip address for docker
    @Bean
    public DefaultDockerClient getDockerClient() {
        try {
            log.info("Getting DefaultDockerClient");
            return DefaultDockerClient.fromEnv()
                    .uri("https://192.168.99.100:2376")
                    .dockerCertificates(new DockerCertificates(Paths.get(path)))
                    .build();
        } catch (DockerCertificateException e) {
            throw new RuntimeException(e);
        }
    }
}
