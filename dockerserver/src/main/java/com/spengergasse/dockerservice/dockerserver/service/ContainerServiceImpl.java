package com.spengergasse.dockerservice.dockerserver.service;

import com.spengergasse.dockerservice.domaincommons.display.container.Container;
import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ExecCreation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ContainerServiceImpl implements ContainerService {

    private final DefaultDockerClient dockerClient;

    @Override
    public Container createContainer(String image, List<String> env) {
        try {
            env.add("APP_PROFILE=prod");
            dockerClient.createContainer(ContainerConfig.builder()
                    .env(env)
                    .image("docker-entity:latest")
                    .build());
            return null;
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Container> listContainers() {
        return null;
    }

    @Override
    public Container stopContainer(String containerId) {
        try {
            dockerClient.stopContainer(containerId, 30);
            return null;
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Container startContainer(String containerId) {
        try {
            dockerClient.startContainer(containerId);
            return null;
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String executeCommand(String containerId, String command) {
        try {
            final String[] commandQuery = {"sh", "-c", "ls"};
            final ExecCreation execCreation;
            execCreation = dockerClient.execCreate(
                    containerId, commandQuery, DockerClient.ExecCreateParam.attachStdout(),
                    DockerClient.ExecCreateParam.attachStderr());
            final LogStream output = dockerClient.execStart(execCreation.id());

            return output.readFully();
        } catch (DockerException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Container getInfos(String containerId) {
        return null;
    }
}
