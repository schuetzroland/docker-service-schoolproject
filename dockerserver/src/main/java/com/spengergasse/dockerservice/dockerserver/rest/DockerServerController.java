package com.spengergasse.dockerservice.dockerserver.rest;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(path = "/api/docker")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DockerServerController {

    private final DefaultDockerClient dockerClient;

    @GetMapping(path = "/test")
    public ResponseEntity<String> createDockerContainer() throws DockerException, InterruptedException {
        log.info("{}",dockerClient.info().architecture());
        return ResponseEntity.ok("Basic test");
    }
}
