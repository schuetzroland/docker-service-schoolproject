package com.spengergasse.dockerservice.frontendserver.persistence;

import com.spengergasse.dockerservice.domaincommons.display.user.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
