package com.spengergasse.dockerservice.frontendserver.service;

import com.spengergasse.dockerservice.domaincommons.display.user.Role;
import com.spengergasse.dockerservice.domaincommons.display.user.User;
import com.spengergasse.dockerservice.frontendserver.persistence.UserRepository;
import com.spengergasse.dockerservice.restcommons.domain.auth.RegisterForm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    private final RoleService roleService;

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User u = userRepository.getUserByUsername(s);
        if (u == null) {
            throw new UsernameNotFoundException("Username not found!");
        }
        return u;
    }

    public void registerUser(RegisterForm registerForm) {
        User u = new User();
        u.setUsername(registerForm.getUsername());
        u.setHashedPassword(passwordEncoder.encode(registerForm.getPassword()));
        Role r = roleService.save(new Role("admin"));
        u.getRoles().add(r);
        userRepository.save(u);
    }

}
