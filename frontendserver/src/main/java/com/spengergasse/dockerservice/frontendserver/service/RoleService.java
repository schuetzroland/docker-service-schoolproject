package com.spengergasse.dockerservice.frontendserver.service;

import com.spengergasse.dockerservice.domaincommons.display.user.Role;
import com.spengergasse.dockerservice.frontendserver.persistence.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleService {

    private final RoleRepository roleRepository;

    public Role save(Role role){
        roleRepository.save(role);
        return role;
    }

}
