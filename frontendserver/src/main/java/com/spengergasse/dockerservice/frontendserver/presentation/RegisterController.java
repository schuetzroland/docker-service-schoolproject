package com.spengergasse.dockerservice.frontendserver.presentation;

import com.spengergasse.dockerservice.frontendserver.service.UserService;
import com.spengergasse.dockerservice.restcommons.domain.auth.RegisterForm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/register")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RegisterController {

    private final UserService userService;

    @ModelAttribute("registerForm")
    public RegisterForm registerForm() {
        return new RegisterForm();
    }

    @GetMapping
    public String registerModal() {
        return "fragments/auth :: register";
    }

    @PostMapping
    public String register(@ModelAttribute RegisterForm registerForm){
        userService.registerUser(registerForm);
        log.error("registered");
        return "index";
    }

}
