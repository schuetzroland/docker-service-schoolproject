package com.spengergasse.dockerservice.frontendserver.presentation;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/containers")
public class ContainerController {

    @GetMapping
    public String containers(){
        return "containers";
    }

}
