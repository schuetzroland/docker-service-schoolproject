package com.spengergasse.dockerservice.restcommons.domain.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContainerCreateRequest {
    private String imageName;
    private String tag;
    private String name;
    private List<String> env;
}
