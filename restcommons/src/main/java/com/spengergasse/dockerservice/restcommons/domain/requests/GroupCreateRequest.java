package com.spengergasse.dockerservice.restcommons.domain.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupCreateRequest {
    private String name;
    private List<Integer> containerIds;
}
