package com.spengergasse.dockerservice.restcommons.domain.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddCustomCommandRequest {
    private String name;
    private String command;
}
