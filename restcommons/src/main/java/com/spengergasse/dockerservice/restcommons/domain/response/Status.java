package com.spengergasse.dockerservice.restcommons.domain.response;

public enum  Status {
    RUNNING, STOPPED
}
